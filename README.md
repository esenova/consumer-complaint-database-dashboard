# **Consumer Complaint Database Dashboard**

## **Introduction**

Consumer Complaint Database is a powerful tool that provides consumers with a platform to report their grievances about goods and services offered by businesses. It is a repository of consumer complaints that is maintained by government agencies and other organizations across different countries. 

# **DATA REPRESENTATION**

## **Complaints by States**

![Figure 1](figures/statemap.png){width="700" height="500"}

The data representation for complaints by state map chart would involve a map of the United States with each state being shaded or colored based on the number of complaints received from that state. The *Figure 1* would show a visual representation of the concentration of complaints in each state. 

Additionally, the chart could include interactive features such as hover-over effects that show the exact number of complaints for each state, or a search function that allows users to find specific states and see their corresponding complaint numbers.

The green and orange shading of each state in the chart represents the state's population size as of the 2018 census. A gradient scale maps each state's population size to a range of colors, with lighter shades of orange or green indicating lower population sizes and darker shades indicating higher population sizes.

## **Treemap Of Complaints By Issues and Products**

![Figure 2](figures/treemap.png){width="700" height="500"}

The financial product or service that is the subject of the consumer complaint is represented by the "Product" attribute. This feature is important because it enables users to pinpoint the specific product or service that is causing problems for customers. Among the many product categories included in the database are mortgage, credit card, student loan, and auto loan.

The complaints by issue, company and product treemap charts are data representations that provide a visual summary of the most common types of complaints filed in the Consumer Complaint Database.

The treemap of complaints by issues and products is a data representation that provides a hierarchical overview of the most common types of complaints filed in the Consumer Complaint Database. This type of data visualization allows users to compare the relative sizes of different categories of complaints and identify patterns and trends within the data.

The treemap is organized in a hierarchical fashion, with larger rectangles representing broad categories of complaints and smaller rectangles representing subcategories. The rectangles are
colored according to the product or issue category they represent, with the intensity of the color indicating the number of complaints in each category.

## **Complaints Performance Metrics**

![Figure 3](figures/piechart.png){width="700" height="750"}

*Figures 3 to 6* display pie charts representing four key attributes of the Consumer Complaint Database: Complaint Status, Consumer Consent, Complaints by Quarter and Timely Response. These attributes are critical components of the database and provide valuable insights into consumer behavior.

- In *Figure 6*, information provides insights into the status of complaints and can be used to analyze the efficiency of complaint handling processes. For example, a high proportion of complaints categorized as Open may indicate that businesses or regulatory agencies need to improve their complaint resolution processes. 

- In *Figure 5*, the Consumer Consent pie chart result displays the proportion of complaints in the Consumer Complaint Database that have received consumer consent to be shared with regulators or third-party organizations versus those that have not. This information is essential for understanding and respecting consumer privacy rights and ensuring that complaints are handled in a legal and ethical manner. 

- In *Figure 4*, the Complaints by Quarter pie chart result displays the proportion of complaints in the Consumer Complaint Database that were filed each quarter. This information provides insights into consumer behavior and can be used to analyze trends in complaints volume. By tracking the number of complaints filed per quarter, businesses and regulatory agencies can identify potential issues and take steps to address them proactively. 

- In *Figure 3*, information provides insights into the effectiveness of complaint resolution processes and can be used to analyze the quality of customer service. By tracking the proportion of complaints that receive a timely response, businesses and regulatory agencies can identify areas for improvement and take corrective actions to enhance their complaint handling processes. 

# **DATA ANALYTICS**

Treemaps are an effective data analytics tool for gaining insights into large datasets. Treemaps enable users to quickly identify patterns, trends, and outliers by visualizing hierarchical data structures. Treemaps are especially useful for analyzing large dataset that contain multiple categories and subcategories, such as consumer complaint database. 

*Figure 4* employs Tableau's *'Total'* function to determine the total number of complaints for each category in the 'Issue' column. The Total function calculates values across the entire dataset, enabling users to drill down into each Issue effectively. This is accomplished by displaying the total number of Sub-Issues, followed by a breakdown of each individual Sub- Issue category, resulting in a more thorough data analysis.

![Figure 4](figures/complaints.png){width="500" height="600"}

# **COMPOSITION**

![Figure 5](figures/dashboard.png){width="800" height="600"}

Effective composition is a crucial aspect of data visualization that encompasses the arrangement of various design elements in a visual display. These elements include the placement of data points, use of color and contrast, and the overall balance and harmony of the design. One of the main benefits of effective composition is that it facilitates the viewer's understanding of the data and helps them extract meaningful insights from it. By presenting the data in a clear and intuitive way, the designer can make it easier for the viewer to identify patterns, trends, and relationships between different data points.

To optimize the dashboard's visual hierarchy, the pie charts were placed at the top, followed by the map and the issue treemap at the bottom. This placement effectively prioritizes the most critical information for the viewer, allowing them to quickly gain a comprehensive understanding of the data. By thoughtfully arranging the data, it was possible to create a seamless user experience that encourages exploration and analysis.

In selecting the color scheme, it was essential to avoid overusing the green color. To achieve this, we incorporated varying shades of orange to create a more harmonious and professional feel. The blend of green and orange helps to provide a balanced color palette that complements the overall design of the dashboard. The use of these colors in the right proportions adds depth and dimension to the data, making it more visually appealing and engaging for the viewer.

In conclusion, the dashboard's design was meticulously crafted to create an intuitive and engaging user experience. The use of green and orange colors, their placement, and the overall composition of the dashboard were all carefully considered to ensure that the data was presented in the most effective and impactful way possible. The result is a visually stunning and informative dashboard that is sure to leave a lasting impression on its viewers.



